from flask import Flask, request, redirect, render_template

app = Flask(__name__)

@app.route('/')
def index():
	return 'Nothing to see here'

@app.route("/hello", methods=['GET'])
def hellouser():
	if request.method == 'GET' and request.args.get('name'):
		username = request.args.get('name')
		return "Hello %s!" % username
	else:
		return "Hello user!", 200

@app.route("/check", methods=['GET'])
def checkGET():
	if request.method == 'GET':
		return "This is a GET request"

@app.route("/check", methods=['POST'])
def checkPOST():
	if request.method == 'POST':
		return "This is a POST request"

@app.route("/check", methods=['PUT'])
def checkPUT():
	if request.method == 'PUT':
		return "This is a PUT request", 405

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)

